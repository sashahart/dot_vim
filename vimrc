" Code in this file won't parse properly unless vi compatibility mode is off.
" That is normally achieved automatically, but not when using 'vim -u' to skip
" system vim configs, which can be useful for debugging or for speed.
" However, we should make sure it does not run twice.
if &compatible
    set nocompatible
endif

" Vim needs this to let us use Unicode characters in this file.
" :help :scriptencoding says it has to be set before scriptencoding.
set encoding=utf-8

" Windows Vim also needs a scriptencoding line to cope with Unicode characters.
if has('win32') || has('win64')
    scriptencoding utf-8
endif

" This file is grouped into functions to make it a little easier to navigate.
" Define Setup() as one big function which will run the rest at end of file.
function! Setup()
    call Suppress_Default_Plugins()
    call Setup_Initial()
    call Setup_Python()
    call Setup_Paths()
    call Setup_Backup()
    call Setup_Swap()
    call Setup_Undo()
    call Setup_Viminfo()
    call Setup_Sessions()
    call Setup_Wrap()
    call Setup_Indent()
    call Setup_Folding()
    call Setup_Completion()
    call Setup_Find()
    call Setup_Grep()
    call Setup_Tags()
    call Setup_Search()
    call Setup_Appearance()
    call Setup_Diff()
    call Setup_Title()
    call Setup_Statusline()
    call Setup_Commandline()
    call Setup_Cursor()
    call Setup_Clipboard()
    call Setup_Bell()
    call Setup_Mouse()
    call Setup_Keys()
    source ~/.vim/plugins.vim
    call Setup_Tab()

    " Load plugins a bit early, so machine.vim can use them
    packloadall
    " Get device-specific settings from machine.vim, which is ignored by git.
    " Things to set here: colorscheme config, colorscheme, font, spelllang...
    source ~/.vim/machine.vim
endfunction

function! Suppress_Default_Plugins()
    " Disable some default plugins that load at startup.
    " e.g. vim is sourcing everything in Debian's $VIMRUNTIME/plugin
    " but many of those things are useless to me
    let g:loaded_vimballPlugin = 1
    let g:loaded_vimball = 1
    let g:loaded_getscriptPlugin = 1
    let g:loaded_logiPat = 1
    let g:loaded_netrwPlugin = 1
    let g:loaded_spellfile_plugin = 1
    let g:loaded_tarPlugin = 1
    let g:loaded_2html_plugin = 1
    let g:loaded_zipPlugin = 1
    let g:loaded_gzip = 1
    let g:loaded_2html_plugin = 1
    " Skip menu.vim, which slows startup and makes menus which claim alt-keys.
    " This will not work from gvimrc - it MUST be in vimrc (anywhere will do).
    set guioptions=M
endfunction

" High-priority commands which should run ASAP.
function! Setup_Initial()
    " When switching buffers, don't unload and lose changes or warn, just hide.
    set hidden
endfunction

function! Setup_Python()
    " Force use of +python3/dyn over +python/dyn if both are present.
    " Otherwise, which gets activated is unpredictable due to has() checks.
    silent! py3 pass
    " Inform vim that we prefer python 3, in case it was not already clear
    set pyxversion=3
endfunction

function! Setup_Paths()
    if has('win32') || has('win64')
        " When starting Windows Vim from a shortcut, pwd is set to $VIMRUNTIME,
        " causing inconvenient permissions errors. We can't just 'cd $HOME' at
        " startup, as that suppresses pwd from command line starts.
        " But empty $PROMPT is a good indicator that Vim ran from a shortcut.
        if $PROMPT ==# '' && $PWD == $VIMRUNTIME
            cd $HOME
        endif

        " Windows Vim needs this to look in ~/.vim rather than ~/vimfiles.
        set runtimepath^=~/.vim
    endif

    " strip down runtimepath.
    " we need $VIMRUNTIME because it contains filetype support.
    set runtimepath=$HOME/.vim,$VIMRUNTIME,$HOME/.vim/after

    " strip down packpath.
    " $VIMRUNTIME/pack contains the dist/opt plugins
    set packpath=$HOME/.vim,$VIMRUNTIME,$HOME/.vim/after

    " Never execute code in modelines, which is a risky thing to do.
    set nomodeline modelines=0
endfunction

function! Setup_Backup()
    " Create backup files before overwriting files.
    set backup
    " After making backup copy, overwrite original to preserve special attribs.
    " This is an intentionally conservative method, e.g. see :help crontab
    set backupcopy=yes
    " Make backup files in given location to reduce clutter in filesystem.
    if has('win32') || has('win64')
        set backupdir=~/.vim/tmp,$TMP
    else
        set backupdir=~/.vim/tmp,/tmp
    endif
endfunction

function! Setup_Swap()
    " Vim periodically writes swap files to allow crash recovery (&swapfile).
    " Write these in a given location to reduce clutter throughout filesystem.
    if has('win32') || has('win64')
        set directory=~/.vim/tmp//,$TMP
    else
        set directory=~/.vim/tmp//,/tmp
    endif
endfunction

function! Setup_Undo()
    if !has('persistent_undo')
        return
    endif
    " Allow undo to work across vim sessions by storing undo history.
    set undofile
    " Store undo files in a specified location to reduce clutter in filesystem.
    set undodir=~/.vim/tmp
    " if we're on unix and can't use the preceding, try the following
    if has('unix')
        set undodir+=/tmp
    endif
endfunction

function! Setup_Viminfo()
    if !has('viminfo')
        return
    endif
    " Store marks for the last x files in viminfo ('x), no registers (<0),
    " disable hlsearch restoration (h). See :help 'viminfo'
    set viminfo='1000,<0,h
    " Store viminfo file under .vim/ instead of ~.
    " Set this last, because anything at end is parsed as part of the path.
    set viminfo+=n~/.vim/tmp/viminfo
endfunction

" Session files allow saving and restoring the state of a running vim session.
function! Setup_Sessions()
    if !has('mksession')
        return
    endif
    set sessionoptions=
    " vim's pwd
    set sessionoptions +=curdir
    " vim's window size in lines and columns
    set sessionoptions +=resize
    " vim's window position
    set sessionoptions +=winpos
    " contents of all tabs, not just the active one
    set sessionoptions +=tabpages
    " empty windows
    set sessionoptions +=blank
    " help windows
    set sessionoptions +=help
    " sizes of windows
    set sessionoptions +=winsize
    " fold state
    set sessionoptions +=folds
    " Intentionally omitted: global/buflocal options, variables, mappings.
    " These things should be set fresh by vimrc or plugins at each startup.
endfunction

" Set options for 'word wrap' or line breaks, and also line joins.
function! Setup_Wrap()
    " Don't wrap lines on screen, since I find that annoying
    set nowrap
    " Default to lines of 79 characters max. Override in ftplugins
    set textwidth=79
    " When set wrap, visually indicate where the line was wrapped.
    " n.b.: Unicode characters require a 'scriptencoding' line.
    if has('linebreak')
        if &encoding ==? 'utf-8'
            set showbreak=…
        endif
    endif
    " when joining commented lines, remove newly-superfluous comment char
    set formatoptions+=j
    " when using 'o' or 'O' to add lines, don't add a comment leader
    " that I will then have to go back and delete
    set formatoptions-=o
    " don't add 2 spaces when joining lines (weird vi-style default)
    set nojoinspaces
endfunction

function! Setup_Indent()
    " How many spaces vim should treat a hard tab as equivalent to.
    " Note: if noexpandtab is set and &tabstop differs from &shiftwidth,
    " Vim will emit a mix of tabs and spaces, likely to cause problems.
    set tabstop=8
    " Number of spaces added by tab and removed by backspace
    set softtabstop=4
    " Indent at start of line, e.g. for >> << and cindent
    set shiftwidth=4
    " Encode indents unambiguously with ASCII 32 instead of ASCII 9 (which
    " does not have a defined width, causing unrecoverable problems when
    " differing assumed tab widths cause illegibility or parse errors).
    " Exceptions can be handled by ftplugin overrides, like for Makefile.
    set expandtab
endfunction

function! Setup_Folding()
    if !has('folding')
        return
    endif
    " Enable folding
    set foldenable
    " No folds start closed
    set foldlevelstart=99
    " I don't normally use the foldcolumn
    set foldcolumn=0
    " Default to indent folding, faster than syntax; ftplugins can override
    set foldmethod=indent
    " Enable VimL folding for augroups, functions, python
    let g:vimsyn_folding = 'afP'
endfunction

function! Setup_Completion()
    if has('wildmenu')
        " Display matches when using tab completion in command mode.
        set wildmenu
        " Globs to always ignore in wildmenu. This affects plugins, etc. too.
        set wildignore=*.pyc
        " Filename suffixes ignored when multiple files match wildcard.
        set suffixes=~,.bak,.swp,.pyc,.pyo,.o
        " Ignore case when completing paths
        set wildignorecase
    endif
    if has('insert_expand')
        " Always show popup menu for insert mode completion. No preview split.
        " (also this is required by mucomplete if you use that)
        set completeopt=menuone
        " Do not show weird preview window supplementary info on completions
        " sort of cool but noisy and not so useful in practice
        set completeopt-=preview
    endif
endfunction

" :find can be used to look for and open a path
function! Setup_Find()
    " &path controls where vim looks when you use 'gf' or ':find somename'.
    set path=
    " Look in directory of current file
    set path+=.
    " Look in pwd
    set path+=,,
    " Look recursively under pwd. This is slow, don't :find or gf in big dirs.
    set path+=**
    " find with a few less keystrokes, and handle spaces in query
    command! -nargs=+ -complete=file_in_path F find <f-args>
endfunction

" :grep is used to search files, no need for ack.vim/ag.vim
function! Setup_Grep()
    if executable('rg')
        set grepprg=rg\ --vimgrep\ --no-heading\ --smart-case\ --color\ never\ --hidden
        set grepformat=%f:%l:%c:%m,%f:%l:%m
    elseif executable('ag')
        set grepprg=ag\ --nogroup\ --nocolor\ --hidden
    endif
    " grep, handle spaces in query, open quickfix automatically.
    " 'silent' suppresses the flood of grep output before done.
    " 'grep!' suppresses the jump to the first output when done.
    command! -nargs=+ G silent grep! <q-args>|copen
    " \*    :grep word under cursor, open quickfix
    nnoremap <silent> <leader>* :exec 'G ' . expand("<cword>")<CR>

    " git grep easily (could also use e.g. Fugitive Ggrep)
    function! GitGrep(pattern)
        let l:old_grepprg = &grepprg
        let &grepprg = 'git grep -n $*'
        execute 'grep! ' . shellescape(a:pattern)
        let &grepprg = l:old_grepprg
    endfunction

    command! -nargs=+ GG silent call GitGrep(<q-args>)|copen
endfunction

" Tag files created by ctags/cscope utilities index code.
" Vim can use these tag files to jump quickly to definitions by name, etc.
function! Setup_Tags()
    " Define where to look for tags files.
    set tags=./.tags,.tags
    if has('cscope')
        " like substituting :cstag for :tag, to hit both cscope and ctags
        set cscopetag
        " :cstag searches cscope first, then tags file
        set cscopetagorder=0
    endif
endfunction

function! Setup_Search()
    " Make / search case-insensitive by default
    set ignorecase
    " Make / search case-sensitive when there are uppercase characters
    set smartcase
    if has('extra_search')
        " Do highlighting-as-you-type when doing searches
        set incsearch
        " Highlight previous search patterns
        set hlsearch
    endif
    " :sub replaces all matches in a line without adding /g, for convenience.
    set gdefault
endfunction

function! Setup_Appearance()
    " Set a clean appearance for the ASCII 'frames' drawn on screen.
    " stl and stlnc are used to fill out active/inactive window statuslines
    set fillchars=stl:\ ,stlnc:\ 
    " vert sets the character used to draw the vertical split separators.
    if &encoding ==? 'utf-8'
        " Unbroken vertical bar using unicode
        set fillchars+=vert:│
    else
        " Unbroken vertical bar using space character and coloring.
        " Some colorschemes do not have contrasting VertSplit bg color,
        " so if you are using spaces, you might need a ':hi VertSplit'
        " override to make the separator visible in some colorschemes.
        set fillchars+=vert:\ 
    endif
    " Change the way that :set list displays special characters
    set listchars=eol:$,tab:>-,trail:█
    " Don't flash matching parentheses when moving over or entering them
    set noshowmatch
    " Allow more work to happen between redraws, for a little speed
    set lazyredraw
    " Visually indicate textwidth as a vertical bar at textwidth+1
    if has('syntax')
        set colorcolumn=+1
    endif
    " Disable conceal, since it confuses me
    if has('conceal')
        set conceallevel=0
    endif
    " use guifg/guibg colors in terminal if it looks like we can
    if $TERM =~# "256color$" 
        set termguicolors
    endif
endfunction

function! Setup_Diff()
    if !has('diff')
        return
    endif
    " Vertically align using space, ignore whitespace, side-by-side
    " don't use diff for hidden buffers
    set diffopt+=filler,iwhite,vertical,hiddenoff
endfunction

function! Setup_Title()
    if !has('title')
        return
    endif

    " Set the title of the window to the value of titlestring
    set title

    " Customize titlestring:
    " distinguish the program (gvim, vim, etc.)
    " if the buffer is named, use its path. otherwise, use cwd.
    exec 'set titlestring='
        \ . '%{has(\"gui_running\")?\"gvim\":\"vim\"}'
        \ . ':\ %{empty(bufname(\"%\"))?'
        \ . 'fnamemodify(getcwd(),\":~\")'
        \ . ':'
        \ . 'expand(\"%:~\")}'
        \ . ' titlelen=70'
endfunction

function! Setup_Statusline()
    if !has('statusline')
        return
    endif
    " Statusline has filename (%t), [+] if modified (%m), [RO] if readonly (%r)
    set statusline=%t%m%r
    if exists('*gutentags#statusline')
        set statusline+=%=%{gutentags#statusline('[',']')}
    endif
endfunction

function! Setup_Commandline()
    " While I prefer cmdheight=1, adding an extra line reduces the chances of
    " overlong messages triggering 'Press ENTER' prompts that interrupt flow.
    set cmdheight=2
    " I prefer more obvious versions of messages, so omit filmnrwxas.
    " oO: Let file read messages overwrite previous ones
    " tT: Try to truncate overlength messages in command line
    " c: don't give insertion mode completion messages, spammy
    set shortmess=oOtTc
endfunction

function! Setup_Cursor()
    " Let cursor change line vs. block in terminal
    if &term =~# "xterm"
        " sent when vim enters insert mode: solid vertical bar
        let &t_SI .= "\<Esc>[6 q"
        " sent when vim enters replace mode: solid underscore
        let &t_SR .= "\<Esc>[4 q"
        " sent when leaving insert or replace mode:
        " normal mode: blinking block
        let &t_EI .= "\<Esc>[0 q"
        " reset cursor when vim exits
        augroup terminal
            autocmd VimLeave * silent !echo -ne "\033]112\007"
        augroup END
    endif
endfunction

function! Setup_Clipboard()
    if has('unix')
        " Connect to X server and populate "* unless unlikely to see X server
        set clipboard=autoselect,exclude:cons\|linux
        " Let Vim share the X clipboard ("+ register)
        if has('unnamedplus')
            set clipboard^=unnamedplus
        endif
    elseif has('win32') || has('win64')
        " In Windows, unnamed is sufficient since there is no distinct "*
        set clipboard=unnamed
    endif
endfunction

function! Setup_Bell()
    " don't ring bell for various trivial events:
    " backspace     e.g. hitting backspace at start of line
    " cursor        e.g. downarrow at bottom of buffer
    " complete      e.g. no dictionary/thesaurus when completing
    " error         e.g. try to join last line
    " esc           hit <Esc> in normal mode (should never be bad)
    " spell         error on spell suggest
    " wildmode      more matches available in cmdline completion
    set belloff+=backspace,cursor,complete,error,esc,spell,wildmode
endfunction

function! Setup_Mouse()
    " Enable mouse in all modes, including in the terminal
    set mouse=a
    " Don't hide mouse cursor during typing
    set nomousehide
endfunction

" Set key mappings which do not depend on plugins.
" Run before sourcing plugins.vim, because some plugins check mappings.
function! Setup_Keys()

    " Don't move across lines just because of first/last character
    set whichwrap=
    " Convenient visual mode indent/dedent
    vnoremap > >gv
    vnoremap < <gv
    " Why hit this twice?
    nnoremap > >>
    nnoremap < <<
    " Cycle through buffers
    noremap <silent> <m-h> :bprev<cr>
    noremap <silent> <m-l> :bnext<cr>
    " Cycle through quickfix items
    nnoremap <silent> <C-Up> :cprev<CR> :norm! zz<CR>
    nnoremap <silent> <C-Down> :cnext<CR> :norm! zz<CR>
    " Cycle through location list items
    nnoremap <silent> <m-Up> :lprev<CR> :norm! zz<CR>
    nnoremap <silent> <m-Down> :lnext<CR> :norm! zz<CR>
    " Replace bufkill, etc.
    command! BW bp<bar>bd #
    " Inspect syntax groups on demand, useful for writing colorschemes
    map <silent> <leader>h :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
    \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
    \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
    " Quickly edit this vimrc
    map <silent> <leader>v :e ~/.vim/vimrc<CR>

    " less delay when hitting ESC to exit insert mode in terminal
    set ttimeout
    set ttimeoutlen=1

endfunction

" Run after sourcing plugins.vim so we can check what exists
function! Setup_Tab()

    " DISABLE any possible loading of supertab
    let loaded_supertab = 1

    " Control what happens when <Tab> is hit in insert mode.
    function! ITab()

        " Try to activate a snippet. If it worked, return early.
        if exists('*UltiSnips#ExpandSnippetOrJump')
            call UltiSnips#ExpandSnippetOrJump()
            if g:ulti_expand_or_jump_res > 0
                return ''
            endif
        endif

        " If popup menu is visible, cycle forward through options.
        " After first tab pulls up menu, too surprising for second e.g. to indent.
        " Keeping this check after the snippet check allows menu selection of
        " snippet triggers which can then be triggered.
        if pumvisible()
            return "\<C-n>"
        endif

        " If at start of line or right after whitespace, just tab as normal.
        let l:pos = getpos('.')
        let l:before = strpart(getline(l:pos[1]), 0, l:pos[2] - 1)
        if strlen(l:before) == 0 || l:before =~# "[ \t]$"
            return "\<Tab>"
        endif

        " Let neocomplete present completion options.
        if exists('*neocomplete#start_manual_complete')
            return neocomplete#start_manual_complete()
        endif

        " Fall back on omnicomplete
        return "\<C-X>\<C-o>"

    endfunction

    " UltiSnips will not work with normal 'imap <expr> <Tab> ITab()';
    " instead, we need this form or we'll get 'Not allowed here: botright new'
    inoremap <silent> <expr> <Tab> "\<C-R>=ITab()<CR>"
endfunction

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call Setup()

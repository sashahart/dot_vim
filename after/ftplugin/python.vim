" Use the Jedi plugin as the omnicompletion source, if it is available.
" This can't be done in ftplugin/python.vim, as that is overridden by
" $VIMRUNTIME/ftplugin/python.vim.

if exists('*jedi#completions')
    setlocal omnifunc=jedi#completions
endif

setl et
setl softtabstop=4
setl shiftwidth=4

nnoremap <leader>r :TernRename<CR>
nnoremap <leader>g :TernDef<CR>
nnoremap <leader>n :TernRefs<CR>

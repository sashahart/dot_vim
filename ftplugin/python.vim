" PEP 8: use spaces instead of ASCII 9.
setlocal expandtab
" PEP 8: 4-space indents. 2/3 is too few for other reasons also.
setlocal tabstop=4
" This should be equal to the tabstop setting.
setlocal shiftwidth=4
" Spaces added by tab and removed by backspace.
setlocal softtabstop=4
" Use shiftwidth at start of line instead of tabstop/softtabstop.
" backspace should also remove shiftwidth spaces.
setlocal smarttab
" Smartindent is deprecated in favor of cindent and filetype indents
setlocal nosmartindent
" cindent should be off by default, Python doesn't indent like C
setlocal nocindent
" but if cindent is ever turned on, these keywords might help a little
setlocal cinwords=class,def,if,elif,else,try,except,finally,for,while,with
" Python comments start with hash.
setlocal commentstring="# %s"
" PEP 8: 79-character lines by default (docstrings are 72, though)
setlocal textwidth=79

" " TODO: connect to cscope if present

if executable('pycscope')
    " pycscope is available on PyPI. Normal cscope won't work well for Python.
    set cscopeprg=pycscope
endif

" If vim was entered from inside a virtualenv, try to
" switch to the same one (must be in ~/.virtualenvs though..)
function! SetVirtualEnv()
	let l:virtualenv_name = fnamemodify($VIRTUAL_ENV, ':t')
        if $VIRTUAL_ENV != '' && exists(':VirtualEnvActivate') > 0 
            exec 'VirtualEnvActivate ' . l:virtualenv_name
        endif
endfunction
call SetVirtualEnv()

" gf searches current dir. But should really search $PYTHONPATH.
" does the virtualenv plugin set this or enable gf on modules?
" sloria/vim-ped - would have to bind to gf, it bypasses &path.
"   this would at least get around the 'slow gf' problem in python files.
"   but it requires pip install ped - then ped is finding the module
" mjbrownie/GetFilePlus
"   this literally attempts 'find' multiple times. glurgh.
" mkomitee/vim-gf-python 
"   this actually invokes python to find (but not import) the module.
"   so vim's python has to have the right PYTHONPATH/venv etc.
"   it doesn't set path.
" literally this should do fine, IF vim's python is the right one.
    " python << EOF
    " import os
    " import sys
    " import vim
    " # add python libs to vim path
    " for p in sys.path:
    "     if os.path.isdir(p):
    "         vim.command(r"setlocal path+=%s" % (p.replace(" ", r"\ ")))
    " EOF
" it is hard to know what sort of sys.path should exist for the file you are
" editing. it would seem we need the virtualenv plugin after all.
" I just can't bring myself to do that.
    " Allow vim's embedded python interpreter to work in a virtualenv.
    " Actually there are not many uses for this other than accessing sys.path
    Plug 'unterzicht/vim-virtualenv'
set path=.
" overall, the plugins seem to be on a consensus where you don't edit &path,
" you just work around it in a totally language-specific way. ugh.
" it might be nice to look at the form of what you are trying to 'gf' and then
" dispatch to different strategies, if you are not just going to set an
" appropriate &path based on the filetype/project type.
" but really - everyone seems to tacitly agree not to use gf. :(

" My code to clean out space at end of lines and blank lines at EOF on save
" TODO: make cleanup into a plugin
if !(&diff) && has('autocmd') && exists('*CleanupWhitespace')
    augroup Cleanup
        autocmd BufWritePre <buffer> :call CleanupWhitespace()
    augroup END
endif

" This functionality is also provided standalone by fs111/pydoc.vim.
nnoremap K :call jedi#show_documentation()<CR>
nnoremap <leader>r :call jedi#rename()<CR>
nnoremap <leader>g :call jedi#goto()<CR>
nnoremap <leader>n :call jedi#usages()<CR>

" let g:python_print_as_function = 1
" let g:python_highlight_space_errors = 1
" let g:python_highlight_indents = 0
" let g:python_indents_style = 2
" let g:python_highlight_string_formatting = 1
" let g:python_highlight_string_format = 1
" let g:python_highlight_doctests = 1
" let g:python_highlight_builtin_objs = 1
" let g:python_highlight_builtin_funcs = 1
" let g:python_highlight_exceptions = 1

" Plugin declarations and associated settings.

" Now putting plugins under pack/plugins/start means they're loaded
" automatically, even if vim-plug is not loaded.

" I'm still using vim-plug to manage plugin updates etc. - just not load them.
" Set this to 1 when you want to run vim-plug to manage plugins.
let managing = 1
if managing
    " Enable vim-plug without keeping it in autoload
    source ~/.vim/pack/plugins/opt/vim-plug/plug.vim
    call plug#begin('~/.vim/pack/plugins/start')
else
    " Load Vug so it can define the Plug command.
    packadd vug
    call vug#init()
endif

" Find project root marked by e.g. '.git' or '.hg'.
" This plugin doesn't do anything unless and until invoked, without config.
Plug 'dbakker/vim-projectroot'
    " Change to project root containing e.g. .git
    map <silent> <Leader>pd
        \ :ProjectRootLCD<CR>
        \ :echo 'Now in ' . getcwd()<CR>
    " Change to directory containing file
    map <silent> <Leader>cd
        \ :exec 'lcd ' . fnamemodify(resolve(expand("%:p")), ':h')<CR>
        \ :echo 'Now in ' . getcwd()<CR>

" Simple directory browser replacing netrw, which I find flaky
Plug 'jeetsukumaran/vim-filebeagle'
    " Do not map <leader>f, I use that
    let g:filebeagle_suppress_keymaps = 1

" Cycle between e.g. code and test files based on patterns.
Plug 'kana/vim-altr'

    " Cycle through alternates, e.g. source -> test -> doc -> ...
    " modify altr#define lines to change the rules for this.
    nmap <C-Right> <Plug>(altr-forward)
    nmap <C-Left> <Plug>(altr-back)

    " What we will call altr#define with after plug#end makes it available.
    let s:altr_python_patterns = [
        \ '%.py',
        \ 'test_%.py',
        \ '%/%.py',
        \ '%/test_%.py',
        \ '%/tests/test_%.py',
        \ '%/test/test_%.py',
        \ '%/../tests/test_%.py',
        \ '%/../test/test_%.py',
        \ '%/../docs/%.rst'
        \ ]
    let s:altr_viml_patterns = [
        \ '%/plugin/%.vim',
        \ '%/autoload/%.vim',
        \ '%/ftdetect/%.vim',
        \ '%/after/%.vim',
        \ '%/doc/%.txt',
        \ ]

" FZF is a fancy framework for quickly narrowing down search results.
" It works well enough to search huge directories on the fly.
" It needs some setup and is not cross-platform, but works in the shell.
if has('unix')
    " If we have FZF from a distro package, use that directly.
    " (Here I use Ubuntu's path, but adjust as needed.)
    " Otherwise, plugin manager can clone it like everything else.
    " NOTE: distro fzf may be old and the fzf plugin may complain...
    let distro_fzf = '/usr/share/doc/fzf/examples'
    if filereadable(distro_fzf)
        Plug distro_fzf
    " Even if we don't have fzf as a distro package,
    " hopefully we already have it installed e.g. with some symlink in
    " ~/.local/bin. So we don't need any post-install steps
    elseif executable('fzf')
        Plug 'junegunn/fzf'
    " If fzf doesn't exist as a command, run its install script
    " to spare the user - this will install fzf somewhere in ~/.vim
    else
        Plug 'junegunn/fzf', { 'do': './install --bin' }
    endif
    " Some extras including :History which I use for MRU
    Plug 'junegunn/fzf.vim'

    " This determines how FZF enumerates files, affecting its speed a lot.
    " --no-messages prevents 'permission denied' stuff from getting in.
    " --files lists files instead of grepping in them (like find).
    " --hidden includes dotfiles (like ~/.zshrc ...)
    " --no-ignore doesn't use files like .gitignore.
    " --glob here avoids searching in .git directories.
    let $FZF_DEFAULT_COMMAND="
        \ rg --no-messages --files --hidden --no-ignore
        \ --glob '!*~'
        \ --glob '!.git/*'
        \ --glob '!/mnt/*'
        \"

    " Fast recursive file search.
    map <leader>z :FZF<CR>

    " FZF used like an MRU plugin
    nnoremap <leader>m :History<CR>

    " Open buffers
    nnoremap <leader>b :Buffers<CR>

endif

" Keep v:oldfiles updated more consistently.
" This is used by the FZF :History function and some others.
Plug 'gpanders/vim-oldfiles'

" Automatically update ctags files. 
" This needs careful minding to avoid gobbling CPU all the time when it
" hits directories like node_modules.
" Note: this plugin only loads usably in a VCS repository.
Plug 'ludovicchabant/vim-gutentags'

    " Switch databases automatically when switching projects
    " within the same Vim instance. Makes gtags-scope more usable.
    " Otherwise, there's no need for this plugin
    Plug 'skywind3000/gutentags_plus'

    " let gutentags skip a little work to find the project root
    let g:gutentags_project_root = [".git"]
    " start generating tags as long as cwd is in a project,
    " e.g. right after opening vim.
    let g:gutentags_generate_on_empty_buffer = 1
    " ignore certain things in ctags.
    " If I don't ignore node_modules, my ~/.vim/.tags is 12 gigs.
    " .tox typically just presents irrelevant results.
    let g:gutentags_ctags_exclude = ["node_modules", ".tox"]
    let g:gutentags_ctags_tagfile = ".tags"

    " prevent gutentags_plus from mapping keys
    let g:gutentags_plus_nomap = 1

" Browse major sites in the current file.
Plug 'majutsushi/tagbar', {'on': 'TagbarToggle'}

    " Keep on left, so quickfix doesn't open in tiny window below
    let g:tagbar_left = 1
    " Default width is too much for my taste
    let g:tagbar_width = 25
    " Don't sort by name, instead order corresponding to location in file.
    let g:tagbar_sort = 0
    " Single click jump to definition
    let g:tagbar_singleclick = 1
    " Enable tagbar table of contents in rst files
    if executable('rst2ctags')
        let g:tagbar_type_rst = {
            \ 'ctagstype': 'rst',
            \ 'ctagsbin' : 'rst2ctags',
            \ 'ctagsargs' : '-f - --sort=yes',
            \ 'kinds' : [
                \ 's:sections',
                \ 'i:images'
            \ ],
            \ 'sro' : '|',
            \ 'kind2scope' : {
                \ 's' : 'section',
            \ },
            \ 'sort': 0,
        \ }
    endif
    if executable('markdown2ctags')
        let g:tagbar_type_markdown = {
            \ 'ctagstype': 'markdown.pandoc',
            \ 'ctagsbin' : 'markdown2ctags',
            \ 'ctagsargs' : '-f - --sort=yes',
            \ 'kinds' : [
                \ 's:sections',
                \ 'i:images'
            \ ],
            \ 'sro' : '|',
            \ 'kind2scope' : {
                \ 's' : 'section',
            \ },
            \ 'sort': 0,
        \ }
    endif

    " Window to show an outline of the file's structure. Unrelated to :tag
    nnoremap <F2> :TagbarToggle<CR>

" Indicate various useful information in the sign column at the left.
" This plugin can do VCS changes but I have it just doing other things.
Plug 'tomtom/quickfixsigns_vim'
    " Indicate marks, quickfix items, and location list items.
    let g:quickfixsigns_classes = ["marks", "qfl", "loc"]
    " Update a little more frequently
    let g:quickfixsigns_events = ["BufReadPost", "CursorHold"]
    " Make marks match SignColumn
    let g:quickfixsigns#marks#texthl = 'SignColumn'

" Indicate parentheses nesting level with colors. Of many rainbow plugins
" that I have tried, I found this the most reliable and easiest to use.
Plug 'luochen1990/rainbow'
    " Enable this plugin by default instead of waiting for activation.
    let g:rainbow_active = 1

" Use % to bounce through if/elseif/endif in many languages
runtime macros/matchit.vim

" For quickly commenting out lines and ranges.
Plug 'tpope/vim-commentary', {'on': 'Commentary'}

    " Shortcuts for commenting out a line range or single line
    vmap <m-3> :Commentary<CR>gv
    nmap <m-3> :Commentary<CR>
    " if you happen to use the nefertiti colorscheme, don't clobber m-3.
    let g:nefertiti_suppress_keymaps = 1

" useful when I get confused about how to traverse undo history to get
" something back
Plug 'sjl/gundo.vim'

" Swap arguments in a function signature or call. I found this plugin more
" robust than vim-twiddle, also like that it doesn't try to do too much.
Plug 'AndrewRadev/sideways.vim'

    " sideways.vim: swap arguments in a function signature or call
    map <leader>a :SidewaysRight<CR>

""Realtime" syntax checking and some automated syntax fixing
Plug 'dense-analysis/ale'

    let g:ale_languagetool_options = '--encoding utf-8 --language en --disable "EN_QUOTES,DASH_RULE,WHITESPACE_RULE"'

    " whitelist linters, not always using any linter that is installed
    " 'text' won't use any unless they are specified
    "   \ 'text': ['proselint', 'languagetool', 'writegood'],
    " languagetool is hard to find now: 
    " https://languagetool.org/download/LanguageTool-stable.zip
    let g:ale_linters = {
        \ 'javascript': ['tsserver', 'eslint'],
        \ 'python': ['pyls', 'pylint', 'vulture'],
        \ 'typescript': ['tsserver', 'eslint'],
        \ 'vim': ['vim-language-server'],
    \ }
    " \ 'text': ['languagetool'],

    " ALE fixers have to be specifically enabled to work in :ALEFix
    " - eslint needs a package.json to exist and parse
    " - what does importjs need and what does it really do?
    let g:ale_fixers = {
        \ 'javascript': ['eslint'],
        \ 'json': ['fixjson'],
        \ 'python': ['isort', 'black'],
        \ 'sh': ['remove_trailing_lines', 'trim_whitespace'],
        \ 'html': ['tidy'],
        \ 'text': [],
        \ 'typescript': ['eslint'],
    \ }

    nnoremap <leader>f :ALEFix<CR>

    let g:ale_html_tidy_options = '-q -e -language en --show-body-only'

" MARKDOWN """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" There are a few markdown previewers, this one is a bit easier to set up
" (e.g. yarn is not required) and can use grip for github accuracy
" Plug 'JamshedVesuna/vim-markdown-preview', {'for': 'markdown'}
    " use grip
    let g:vim_markdown_preview_github=0
    " trying to make it work
    let g:vim_markdown_preview_use_xdg_open=1

" this works ok
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

" PYTHON """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Python indent script better than default, doesn't create PEP 8 problems
Plug 'Vimjas/vim-python-pep8-indent', {'for': 'python'}

" Also use % in Python buffers, e.g. if/elif/else
Plug 'vim-scripts/python_match.vim', {'for': 'python'}

" Provide text objects for Python: e.g., vaf vif vac vic
Plug 'bps/vim-textobj-python', {'for': 'python'}
    " required for textobj-python
    Plug 'kana/vim-textobj-user'

" JAVASCRIPT """"""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Syntax script which can handle ES6 syntax - just for things like =>
Plug 'othree/yajs.vim', {'for': 'javascript'}

" javascript syntax highlight - needed by vim-jsx
Plug 'pangloss/vim-javascript', {'for': 'javascript'}
Plug 'mxw/vim-jsx', {'for': 'javascript'}
    " only apply to .jsx files
    let g:jsx_ext_required = 1

Plug 'maksimr/vim-jsbeautify'

" RESTRUCTUREDTEXT """"""""""""""""""""""""""""""""""""""""""""""""""""

" Preview rst files. Needs a python module installed:
" pip install --user https://github.com/Rykka/instant-rst.py/archive/master.zip
Plug 'gu-fan/InstantRst'
    let g:instant_rst_slow = 0
    let g:instant_rst_browser = ''
    let g:instant_rst_localhost_only = 1
    let g:instant_rst_bind_scroll = 1

" SQL """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Plug 'lifepillar/pgsql.vim'
    " default .sql files to be treated as postgresql
    " :h ft_sql.txt
    let g:sql_type_default = 'pgsql'

" TYPESCRIPT """"""""""""""""""""""""""""""""""""""""""""""""""""""""""

Plug 'leafgarland/typescript-vim', {'for': 'typescript'}

" OTHER FILETYPES """""""""""""""""""""""""""""""""""""""""""""""""""""

" Configs for the nginx http daemon
Plug 'vim-scripts/nginx.vim', {'for': 'nginx'}

" Jinja template highlighting more detailed than stock
Plug 'Glench/Vim-Jinja2-Syntax', {'for': 'jinja'}

" Always highlight the surrounding tags while editing HTML
Plug 'Valloric/MatchTagAlways', {'for': 'html'}

" machine-specific plugins (since machine.vim is not before plug#end)
source ~/.vim/machine_plugins.vim

if managing
    call plug#end()
endif

" utility function to see if a plugin was loaded, from
" https://vi.stackexchange.com/questions/10939/how-to-see-if-a-plugin-is-active
" this is useful because the presence of many of these plugin defined functions
" can't be tested with exists() this early in the load process.
function! PlugLoaded(name)
    return (
        \ has_key(g:plugs, a:name) &&
        \ isdirectory(g:plugs[a:name].dir) &&
        \ stridx(&rtp, g:plugs[a:name].dir) >= 0)
endfunction

" -----------------------------------------------------------------------------
" Put any configuration that requires calling plugin functions below.
" Uses of functions defined by plugins must come after plug#end, or else
" we'll get 'E117: Unknown function'. This prevents calls to configuration
" functions being located with Plug lines above.
" -----------------------------------------------------------------------------

if PlugLoaded("vim-altr")
    call altr#remove_all()
    call call('altr#define', s:altr_python_patterns)
    call call('altr#define', s:altr_viml_patterns)
endif

if PlugLoaded("ale")
    " vim-language-server needs a custom definition as LSP linter for ALE.
    call ale#linter#Define('vim', {
       \ 'name': 'vim-language-server',
       \ 'lsp': 'stdio',
       \ 'executable': 'vim-language-server',
       \ 'command': '%e --stdio',
       \ 'project_root': '/path/to/root_of_project',
    \ })
    " Set ALE as default completer. ftplugins should override this in buffers
    " checking for the existence of the function directly won't work
    set omnifunc=ale#completion#OmniFunc
endif

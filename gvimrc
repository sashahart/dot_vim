" GUI-specific options for Vim that don't need to be set early in vimrc.  

" Use GUI tabs.
" intentionally omit other options e.g. toolbar:T, scrollbars:rRlLb, menubar:m
set guioptions+=e
" Change blink frequency to about 3 blinks per 2 seconds (C64 feel)
set guicursor+=a:blinkwait334-blinkon333-blinkoff333
" Use simple space-saving tab names
set guitablabel=%t
" When using a file browser, start at vim's pwd.
set browsedir=current
" Use GUI dialogs to warn about e.g. unsaved changes unless using !
set confirm

" Set $TERM so :terminal handles 256 colors
" if we're already in a terminal, we want to use the existing $TERM
" this may also not need to be done if gvim was launched from xterm
let $TERM = "xterm-256color"
